package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    Button submit;
//    TextView nim, nama;
    ArtikelAdapter adapter;
    public static String TAG = "RV1";
    Intent intent;

    final static String source = "https://medium.com/feed/tag/programming";
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        AsyncExample task = new AsyncExample(rv1);
        task.execute();
    }

    class AsyncExample extends AsyncTask<String, String, List<String>> {
        RecyclerView rv1;

        public AsyncExample(RecyclerView rv1) {
            this.rv1 = rv1;
        }

        @Override
        protected List<String> doInBackground(String... strings) {
            RssParser parser = new RssParser();
            try {
                String xml = parser.loadRssFromUrl(source);
                List<String> data = parser.parseRssFromUrl(xml);
                return data;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            super.onPostExecute(strings);
            adapter = new ArtikelAdapter(MainActivity.this, strings);
            rv1.setAdapter(adapter);
            rv1.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        }
    }
}