package id.ac.ub.papb.recycler1;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ArtikelAdapter extends RecyclerView.Adapter<ArtikelAdapter.ArtikelViewHolder> {
    LayoutInflater inflater;
    Context _context;
    List<String> data;

    public ArtikelAdapter(Context _context, List<String> data) {
        this._context = _context;
        this.data = data;
        this.inflater = LayoutInflater.from(this._context);
    }

    @NonNull
    @Override
    public ArtikelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new ArtikelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtikelViewHolder holder, int position) {
        String judul = data.get(position);
        holder.tvJudul.setText(judul);
    }

    @Override
    public int getItemCount() {
        Log.d(MainActivity.TAG,"Jumlah data "+data.size());
        return data.size();
    }

    class ArtikelViewHolder extends RecyclerView.ViewHolder {
        TextView tvJudul;

        public ArtikelViewHolder(@NonNull View itemView) {
            super(itemView);
            tvJudul = itemView.findViewById(R.id.tvNim);
        }
    }
}